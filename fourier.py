import cmath

### Diskrétní Fourierova transformace.
def dft(pole):
	N = len(pole)
	roz = range(0, N)
	res = [0]*N
	for f in roz:
		base = (-2j*cmath.pi*f)/N
		for k in roz:
			res[f] += pole[k]*cmath.exp(base*k)
	return res
### Konec dft

### Inversní diskrétní Fourierova transformace.
def idft(pole):
	N = len(pole)
	roz = range(0, N)
	res = [0]*N
	for k in roz:
		base = (2j*cmath.pi*k)/N
		for f in roz:
			res[k] += pole[f]*cmath.exp(base*f)
		res[k] /= N
	return res
### Konec idft

### Výpočet transformace...
X = [1, 2, 10, 4, 5, 100]
Y = dft (X)
Z = idft(Y)
