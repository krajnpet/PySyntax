### Třída datum.
class Datum(object):
	## Getery atributů makra.
	def getMonth(self):
		return self.month
	def getYear(self):
		return self.year
	def getDay(self):
		return self.day

	## Konstruktor.
	def __init__(self):
		self.year  = 2016
		self.month = 4
		self.day   = 10
		return
	## Výpis datumu.
	def tiskni(self):
		print (u"Datum je %d.%d.%d\n" %\
					(self.day, self.month, self.year))
### Konec třídy Datum.
